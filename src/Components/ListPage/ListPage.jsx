import React, { useState, useEffect } from "react";
import { Form, Button, Card, Alert, Container } from "react-bootstrap";
import Loader from "../../Core/Loader/Loader";
import { useHistory } from "react-router-dom";
import LOGO from "../../assets/images/success.png";


//Listing of properties
const ListPage = () => {
  const history = useHistory();
  const [data, setData] = useState({
    error: null,
    list: [],
    newList: [],
    bedrooms: [],
    bedroom: -1,
    neighbourhoods: [],
    neighbourhood: "-1",
    minPrice: "",
    maxPrice: "",
    count: 0,
    loading: true,
  })
  
  const {
    bedrooms,
    neighbourhoods,
    newList,
    count,
    loading,
    minPrice,
    maxPrice
  } = data;

  
  useEffect(() => {
    setTimeout(function(){ setData({ ...data, loading: false }); }, 2000);
    
  }, [])

  
  return (
    <div>
      {loading ? (
        <Loader />
      ) : (<div>
        <Container
        className="login-container p-0"
      >
           {/* <div className="w-100 success-card-wrapper"> */}
          <Card className="d-flex align-items-center justify-content-center  w-100 success-card" style={{backgroundColor: "#292929", height: "100vh", color: "#fff"}}>
            <Card.Body>
              <div className="text-center m-5" style={{color: "#fff"}}>
                <img src={LOGO} style={{ height: "40%", width: "40%", borderRadius: "20%" }} />
                <h4 className="m-4 mt-5" style={{color: "#fff"}}>SUCCESS</h4>
              </div>
              <div className="text-center m-5">
                <h4 className="m-2 mt-5" >CONGRATULATIONS</h4>
                <h4 className="mt-4" >Hi Android, You have successfully logged in on your TV. Enjoy our content.</h4>
              </div>
            </Card.Body>
          </Card>
         
        {/* </div> */}
      </Container>
      </div>)}
      </div>
        
  );
}

export default ListPage;
