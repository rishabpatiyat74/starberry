import React from "react";

const RightPane = ({ detail }) => {
  const createMarkup = (detail) => {
    return { __html: detail.Description };
  }

  //Right Pane for the property details
  return (
    <div className="row mt-4 mx-2">
      <div className=" d-flex flex-row justify-content-end align-items-start mb-3">
        <i className="fas fa-share-alt mx-2"></i>
        <i className="far fa-heart"></i>
      </div>
      <hr />
      <div className="d-flex flex-row justify-content-start align-items-center">
        <h3 className="m-0 fw-bolder me-2">€ {detail.Price}</h3>
        <p className="m-0 mx-1">{detail.Bedrooms} bed</p> |{" "}
        <p className="m-0 mx-1">{detail.Floor_Area} sqm</p>
      </div>
      <a className="text-warning my-4 fw-bold">
        <i className="fas fa-home me-2"></i>Please contact us
        </a>
      <button className="w-100 btn btn-dark mt-3 py-2">CONTACT AGENT</button>
      <h5 className="mt-3">FACTS & FEATURES</h5>
      <hr />
      <div className="row my-3">
        <div className="col">
          <h6 className="m-0 fw-bolder">Neighbourhood</h6>
        </div>
        <div className="col">
          <h6 className="m-0 text-decoration-underline">{detail.neighbourhood}</h6>
        </div>
      </div>
      <div className="row my-3">
        <div className="col">
          <h6 className="m-0 fw-bolder">Price per sqm</h6>
        </div>
        <div className="col">
          <h6 className="m-0 text-decoration-underline">€ {detail.Price_Per_Sqm}</h6>
        </div>
      </div>
      <div className="row my-3">
        <div className="col">
          <h6 className="m-0 fw-bolder">Brochure</h6>
        </div>
        <div className="col">
          <h6 className="m-0 text-decoration-underline">Download Brouchure</h6>
        </div>
      </div>
      <div className="row my-3">
        <div className="col">
          <h6 className="m-0 fw-bolder">Floor plan</h6>
        </div>
        <div className="col">
          <h6 className="m-0 text-decoration-underline">View Floorplan</h6>
        </div>
      </div>
      <p className="my-3 fs-6" dangerouslySetInnerHTML={createMarkup(detail)}></p>
    </div>
  );
}

export default RightPane;
