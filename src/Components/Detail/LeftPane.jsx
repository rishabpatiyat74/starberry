import React from "react";


// Left Pane for Property Images
const LeftPane = ({ detail }) => {
  return (
    <div>
      <div className="row m-3 ">
        <div
          style={{
            height: "calc(100vh/2.5)",
            backgroundImage: `url(${detail?.Images && detail?.Images[0]?.url})`,
            backgroundPosition: "center",
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
          }}
        />
        <div className="col-6 mt-3">
          <div
            style={{
              height: "calc(100vh/3.5)",
              backgroundImage: `url(${detail?.Images && detail?.Images[1]?.url})`,
              backgroundPosition: "center",
              backgroundRepeat: "no-repeat",
              backgroundSize: "cover",
            }}
          />
        </div>
        <div className="col-6 mt-3">
          <div
            style={{
              height: "calc(100vh/3.5)",
              backgroundImage: `url(${detail?.Images && detail?.Images[2]?.url})`,
              backgroundPosition: "center",
              backgroundRepeat: "no-repeat",
              backgroundSize: "cover",
            }}
          />

        </div>
      </div>
    </div>
  );
}

export default LeftPane;
