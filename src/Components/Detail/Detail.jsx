import React, { useState, useEffect } from "react";
import LeftPane from "./LeftPane";
import RightPane from "./RightPane";
import { useLocation } from "react-router-dom";


//Complete property details
const Detail = () => {
  const location = useLocation();
  const [data, setData] = useState({
    detail: {},
    isLoaded: false,
    items: [],
  })

  useEffect(() => {
    setData({ ...data, detail: location.state.data });
  }, [])

  return (
    <div className="row justify-content-around">
      <h2 className="text-center my-4">Property Details</h2>
      <div className="col-md-6 col-12">
        <LeftPane detail={data.detail} />
      </div>
      <div className="col-md-5 col-12">
        <RightPane detail={data.detail} />
      </div>
    </div>
  );
}

export default Detail;
