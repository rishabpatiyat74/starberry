import React, { useRef, useState } from "react";
import { Form, Button, Card, Alert, Container } from "react-bootstrap";
import { Link, useHistory } from "react-router-dom";

import "./Login.scss";
import LOGO from "../../assets/images/syna.jpg";

export default function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");
  const history = useHistory();

  //credential check for login
  async function handleSubmit(e) {
    e.preventDefault();
    if (email.length === 0) {
      setError("Please enter a email Address");
    }
    if (password.length === 0) {
      setError("Please enter a email Address");
    }
    if (email === "android@synamedia.com" && password === "synamedia") {
      localStorage.setItem("userLogged", true);
      history.push("/success");
    } else {
      setError("EmailID and Password Mismatch");
    }
  }

  return (
    <>
      <Container
        className="d-flex align-items-center justify-content-center login-container"
        style={{}}
      >
        <div className="w-100 login-card-wrapper">
          <Card>
            <Card.Body>
              <div className="text-center">
                <img src={LOGO} style={{ height: "20%", width: "20%" }} />
              </div>
              {error && <Alert variant="danger">{error}</Alert>}
              <Form onSubmit={handleSubmit}>
                <Form.Group id="email">
                  <Form.Label>Email</Form.Label>
                  <Form.Control
                    type="email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                  />
                </Form.Group>
                <Form.Group id="password">
                  <Form.Label className="mt-2">Password</Form.Label>
                  <Form.Control
                    type="password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                  />
                </Form.Group>
                <div className="text-danger">{error}</div>
                <div className="text-center">
                  <Button
                    className="w-50 mt-3"
                    style={{ backgroundColor: "#292929" }}
                    type="submit"
                  >
                    LOG IN
                  </Button>
                </div>
                <div className = "text-center mt-3">
                  or Login Using
                </div>
                <div class="text-center mt-3">
                <i class="fab fa-facebook fa-lg px-2" onClick={() => history.push("/success")}></i>
                <i class="fab fa-google px-2" onClick={() => history.push("/success")}></i>
                <i class="fab fa-apple fa-lg px-2" onClick={() => history.push("/success")}></i>
                 </div>
              </Form>
            </Card.Body>
          </Card>
        </div>
      </Container>
    </>
  );
}
