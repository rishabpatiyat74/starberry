import React, { Fragment, Suspense } from "react";
import { BrowserRouter as Router, Redirect, Route, Switch } from "react-router-dom";

//Component Imports
import Authentication from "./Core/Authentication";
import Routes from "./Core/Routes";

//Lazy Imports
const Login = React.lazy(() => import("./Components/Login/Login"));
const Detail = React.lazy(() => import("./Components/Detail/Detail"));
const ListPage = React.lazy(() => import("./Components/ListPage/ListPage"));

//Protected/Authenticated Routes
const ProtectedRoute = () => {
  const routes = [
    {
      path: "/success",
      component: ListPage,
      exact: true,
    },
    {
      path: "/detail/:id",
      component: Detail,
      exact: false,
    },
  ];
  return (
    <>
      {/* <Route>   */}
      <Suspense fallback={<>Please wait.....</>}>
        <Routes routes={routes} redirectTo="/" />
      </Suspense>
    </>
  );
};

const App = () => {
  return (
    <Fragment>
      <Router>
        <Suspense fallback={<div>Loader Component</div>}>
          <Switch>
            <Route path="/" component={Login} exact />
            <Authentication>
              <ProtectedRoute />
            </Authentication>
            <Redirect to="/" />
          </Switch>
        </Suspense>
      </Router>
    </Fragment>
  );
}

export default App;
